Assessment: More HTML Tags
In the last assessment, you created your own portfolio web page demonstrating headers, paragraphs, and links (<H1>, <P>, and <A HREF="..."> tags).

Today we want you to add more content to this page, making use of all the new markup you have learned. Please use the following features:

Ordered and bulleted lists
Bold and italic text
Summary and details tags
Separating content using divisions, sections, and horizontal rules
Configuring the title of the page or tab
Embedding images
Showing one or more special characters using HTML Entities
All of the items above are required, but feel free to use other tags or introduce rudimentary styling if you like!

Create a new folder in your Cloud9 environment and name it as 02-more-html-tags. and submit your Cloud9 environment url in canvas.